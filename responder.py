#!/bin/env python3

import paho.mqtt.client as mqtt
import os
import socket
import ssl
import json
import logging
import wx
import wx.adv
import configparser

base_dir = os.path.dirname(os.path.realpath(__file__)) + "/"

# Configuration
config = configparser.ConfigParser()
config.read(base_dir + "config.ini")

# Logging configuration
log = logging.getLogger(config.get('config', 'device'))
log_formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
log.setLevel(logging.DEBUG)

log_stream_handler = logging.StreamHandler()
log_file_handler = logging.FileHandler(base_dir + 'responder.log')
log_file_handler.setFormatter(log_formatter)
log_stream_handler.setFormatter(log_formatter)
log.addHandler(log_file_handler)
log.addHandler(log_stream_handler)

log.debug("CA File: " + config.get('certs', 'ca'))
log.debug("Certificate: " + config.get('certs', 'cert'))
log.debug("Key File: " + config.get('certs', 'key'))


class MQTTClient:
    def __init__(self, name = config.get('config', 'device')):
        self.mqtt_client = mqtt.Client(client_id=name)
        self.mqtt_client.connected_flag=False
        self.mqtt_client.tls_set(
            base_dir + config.get('certs', 'ca'),
            certfile = base_dir + config.get('certs', 'cert'),
            keyfile = base_dir + config.get('certs', 'key'),
            tls_version=ssl.PROTOCOL_TLSv1_2)
        self.connected_flag = False

    def connect(self):
        log.info("Connecting MQTT Client")
        self.mqtt_client.connect(config.get('config', 'host'), port=8883, keepalive=60)
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_subscribe = self.on_subscribe
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.mqtt_client.loop_start()

    def disconnect(self):
        log.info("Disconnecting MQTT Client")
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        log.info("Connection established.")
        log.debug("Setting `connected_flag` to True")
        self.connected_flag=True
        # Subscribe on all necessery topics
        log.info("Subscribing to " + config.get('config', 'topic'))
        self.mqtt_client.subscribe(config.get('config', 'topic'))

    def on_disconnect(self, client, userdata, rc):
        log.info("MQTT Client disconnected")
        log.debug("Setting `connected_flag` to False")
        self.connected_flag=False

    # On subscribe
    def on_subscribe(self, client, userdata, mid, granted_qos):
        log.info("Subscribed: {}({}) data: {}".format(mid, granted_qos, userdata))

    # On message
    def on_message(self, client, userdata, msg):
        command = msg.payload.decode('utf-8')
        log.info("Received command: {}".format(command))
        log.debug("Running command: " + base_dir + "/actions/" + command)
        os.system(base_dir + "actions/" + command)

class TaskBarIcon(wx.adv.TaskBarIcon):
    def __init__(self, frame):
        self.frame = frame
        wx.adv.TaskBarIcon.__init__(self)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.on_set_icon)
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_set_icon, self.timer)
        self.on_set_icon()
        self.timer.Start(60 * 1000)

    def on_set_icon(self, event_object=None):
        if client.connected_flag:
            icon = "connected.svg"
            tooltip = "Connected"
        else:
            icon = "disconnected.svg"
            tooltip = "Disconnected"
        icon_path = base_dir + "/icons/" + icon
        icon = wx.Icon(icon_path)
        self.SetIcon(icon, tooltip)

    def CreatePopupMenu(self):
        menu = wx.Menu()
        connect_m = wx.MenuItem(menu, wx.NewId(), 'Connect')
        menu.Bind(wx.EVT_MENU, self.on_connect, id=connect_m.GetId())
        menu.Append(connect_m)
        disconnect_m = wx.MenuItem(menu, wx.NewId(), 'Disconnect')
        menu.Bind(wx.EVT_MENU, self.on_disconnect, id=disconnect_m.GetId())
        menu.Append(disconnect_m)
        menu.AppendSeparator()
        quitm = wx.MenuItem(menu, wx.NewId(), 'Quit')
        menu.Bind(wx.EVT_MENU, self.on_quit, id=quitm.GetId())
        menu.Append(quitm)
        return menu

    def on_connect(self, event):
        client.connect()
        self.on_set_icon()

    def on_disconnect(self, event):
        client.disconnect()
        self.on_set_icon()

    def on_quit(self, event):
        self.RemoveIcon()
        wx.CallAfter(self.Destroy)
        self.frame.Close()


if __name__ == '__main__':
    client = MQTTClient()
    client.connect()
    app = wx.App()
    frame=wx.Frame(None)
    TaskBarIcon(frame)
    app.MainLoop()